"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var program = require("commander");
var AtlasBreaker_1 = require("./AtlasBreaker");
var myPackage = require('../package.json');
var rmQuotes = function (val) {
    var rst = val.match(/(['"])(.+)\1/);
    if (rst)
        return rst[2];
    return val;
};
program
    .version(myPackage.version, "-v, --version")
    .option("-s, --src [path]", "Source sequence images path.", rmQuotes)
    .option("-o, --output [path]", "Sequence frame images output path.", rmQuotes)
    .parse(process.argv);
var opts = program.opts();
console.log("cmd options: " + JSON.stringify(opts));
var ab = new AtlasBreaker_1.AtlasBreaker();
ab.start(opts.src, opts.output);
