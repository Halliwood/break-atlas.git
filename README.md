# break-atlas

#### 介绍

帮助你分解Laya打包的图集。

#### 安装教程

```
npm i break-atlas
```

#### 使用说明

1. 分解指定的单个图集
```
break-atlas -s D:/layaproj/bin/res/atlas/myAtlas.atlas
```

2. 分解指定的单个图集，并指定输出路径
```
break-atlas -s D:/layaproj/bin/res/atlas/myAtlas.atlas -o D:/rawpngs
```

3. 分解指定目录下的所有图集
```
break-atlas -s D:/layaproj/bin/res/atlas
```

4. 分解当前目录下的所有图集
```
break-atlas
```